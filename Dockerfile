ARG PKG_DIR="/app"
ARG SRV_DIR="server"
ARG SRC_DIR="content"
ARG EXPOSE_PORT="8080"
ARG EXPOSE_PORT_PROTOCOL="tcp"
ARG USERNAME="archer"


FROM golang:alpine as gopiler
ARG SRV_DIR
ARG PKG_DIR
RUN mkdir -p ${PKG_DIR}
COPY ${SRV_DIR} ${PKG_DIR}
WORKDIR ${PKG_DIR}
RUN go version && \
    go mod tidy && \
    mkdir -p ${PKG_DIR}/build && \
    go build -o build/server.go

FROM alpine as runner
ARG PKG_DIR
ARG SRC_DIR
ARG USERNAME
ARG EXPOSE_PORT
ARG EXPOSE_PORT_PROTOCOL
COPY --from=gopiler ${PKG_DIR}/build ${PKG_DIR}
COPY ${SRC_DIR}/ ${PKG_DIR}/static
RUN apk add shadow tree && \
    chmod 544 ${PKG_DIR}/server.go && \
    useradd ${USERNAME} && \
    chown -R ${USERNAME}:${USERNAME} ${PKG_DIR}
USER ${USERNAME}
WORKDIR ${PKG_DIR}
RUN tree .
EXPOSE ${EXPOSE_PORT}/${EXPOSE_PORT_PROTOCOL}
CMD ./server.go

